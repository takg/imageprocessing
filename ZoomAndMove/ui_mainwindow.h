/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Thu 17. Feb 23:45:10 2011
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actionOpen;
    QAction *actionExit;
    QAction *actionZoomIn;
    QAction *actionZoomOut;
    QAction *actionPlay;
    QAction *actionPause;
    QAction *actionNext;
    QAction *actionPrevious;
    QWidget *centralWidget;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QString::fromUtf8("MainWindowClass"));
        MainWindowClass->resize(600, 400);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/new/prefix1/icons/read_icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindowClass->setWindowIcon(icon);
        actionOpen = new QAction(MainWindowClass);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/new/prefix1/icons/file-open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionExit = new QAction(MainWindowClass);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/new/prefix1/icons/exit.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon2);
        actionZoomIn = new QAction(MainWindowClass);
        actionZoomIn->setObjectName(QString::fromUtf8("actionZoomIn"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/new/prefix1/icons/zoom-in-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionZoomIn->setIcon(icon3);
        actionZoomOut = new QAction(MainWindowClass);
        actionZoomOut->setObjectName(QString::fromUtf8("actionZoomOut"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/new/prefix1/icons/zoom-out-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionZoomOut->setIcon(icon4);
        actionPlay = new QAction(MainWindowClass);
        actionPlay->setObjectName(QString::fromUtf8("actionPlay"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/new/prefix1/icons/play-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionPlay->setIcon(icon5);
        actionPause = new QAction(MainWindowClass);
        actionPause->setObjectName(QString::fromUtf8("actionPause"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/new/prefix1/icons/pause-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionPause->setIcon(icon6);
        actionNext = new QAction(MainWindowClass);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/new/prefix1/icons/next-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionNext->setIcon(icon7);
        actionPrevious = new QAction(MainWindowClass);
        actionPrevious->setObjectName(QString::fromUtf8("actionPrevious"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/new/prefix1/icons/prev-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrevious->setIcon(icon8);
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(90, 30, 201, 141));
        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        MainWindowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindowClass->setStatusBar(statusBar);

        mainToolBar->addAction(actionOpen);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionZoomIn);
        mainToolBar->addAction(actionZoomOut);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionPlay);
        mainToolBar->addAction(actionPause);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionPrevious);
        mainToolBar->addAction(actionNext);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionExit);
        mainToolBar->addSeparator();

        retranslateUi(MainWindowClass);

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindowClass", "Open", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionOpen->setToolTip(QApplication::translate("MainWindowClass", "Open files", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionOpen->setShortcut(QApplication::translate("MainWindowClass", "Ctrl+O", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindowClass", "Exit", 0, QApplication::UnicodeUTF8));
        actionZoomIn->setText(QApplication::translate("MainWindowClass", "ZoomIn", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionZoomIn->setToolTip(QApplication::translate("MainWindowClass", "Zoom In", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionZoomIn->setShortcut(QApplication::translate("MainWindowClass", "Ctrl+=", 0, QApplication::UnicodeUTF8));
        actionZoomOut->setText(QApplication::translate("MainWindowClass", "ZoomOut", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionZoomOut->setToolTip(QApplication::translate("MainWindowClass", "Zoom Out", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionZoomOut->setShortcut(QApplication::translate("MainWindowClass", "Ctrl+-", 0, QApplication::UnicodeUTF8));
        actionPlay->setText(QApplication::translate("MainWindowClass", "Play", 0, QApplication::UnicodeUTF8));
        actionPlay->setShortcut(QApplication::translate("MainWindowClass", "Ctrl+P", 0, QApplication::UnicodeUTF8));
        actionPause->setText(QApplication::translate("MainWindowClass", "Pause", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionPause->setToolTip(QApplication::translate("MainWindowClass", "Pause", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionPause->setShortcut(QApplication::translate("MainWindowClass", "Ctrl+S", 0, QApplication::UnicodeUTF8));
        actionNext->setText(QApplication::translate("MainWindowClass", "Next", 0, QApplication::UnicodeUTF8));
        actionNext->setShortcut(QApplication::translate("MainWindowClass", "Right", 0, QApplication::UnicodeUTF8));
        actionPrevious->setText(QApplication::translate("MainWindowClass", "Previous", 0, QApplication::UnicodeUTF8));
        actionPrevious->setShortcut(QApplication::translate("MainWindowClass", "Left", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindowClass", "Image to be shown on this Label", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
