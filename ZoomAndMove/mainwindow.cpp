#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QTimer>
#include <QMessageBox>

static const double IMAGE_SCALE_RATIO = 2;
static const int DELAY_MILLI_SEC = 2000;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindowClass)
{
    ui->setupUi(this);
    hide();
    this->showMaximized();
    ui->label->setText("");
    ui->label->setGeometry(this->geometry());
    show();

    connect(ui->actionOpen,SIGNAL(triggered()),this,SLOT(onOpen()));
    connect(ui->actionZoomIn,SIGNAL(triggered()),this,SLOT(onZoomIn()));
    connect(ui->actionZoomOut,SIGNAL(triggered()),this,SLOT(onZoomOut()));
    connect(ui->actionPlay,SIGNAL(triggered()),this,SLOT(onPlay()));
    connect(ui->actionPause,SIGNAL(triggered()),this,SLOT(onPause()));
    connect(ui->actionNext,SIGNAL(triggered()),this,SLOT(onNext()));
    connect(ui->actionPrevious,SIGNAL(triggered()),this,SLOT(onPrevious()));
    connect(ui->actionExit,SIGNAL(triggered()),this,SLOT(close()));

    fileDialog.setFileMode(QFileDialog::ExistingFiles);
    slideShow = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onOpen()
{
    QStringList fileNames;

    if (fileDialog.exec())
    {
        fileNames = fileDialog.selectedFiles();
        nLoop = 0;
    }

    if (fileDialog.selectedFiles().count() > 0)
    {
        pixmap = QPixmap(fileDialog.selectedFiles()[0]);
        factor = 1.0; // no Zoom
        scrollY = 0;  // start the image from top
        drawImage();
    }

    return;    
}

void MainWindow::drawImage()
{
    if (nLoop < fileDialog.selectedFiles().count())
    {
        QRect rect = this->geometry();

        if ((this->height()+scrollY) < pixmap.height())// condition for vertical scroll of image
        {
            rect.setX(0);
            rect.setY(0);

            // set the pixmap for scrolling
            int x = 0;

            // scrolling parameters X
            if (pixmap.width() > this->geometry().width())
            {
                // take the middle of the image
                x = pixmap.width() - this->geometry().width();
                x /= 2;
            }
            else
            {
                x = 0;
                rect.setLeft((rect.width()-pixmap.width())/2);
            }

            ui->label->setGeometry(rect);

            scrollY += 2;

            // increment the scroll position accordingly
            if (pixmap.height() > (ui->label->height()+scrollY) && (scrollY > 0))
            {
                scrollY += 2;
            }
            else // rescroll from the top
            {
                ;//scrollY = 0;
            }

            QPixmap newPixmap = pixmap.copy(
                    x,
                    scrollY,
                    this->geometry().width(),
                    this->geometry().height());
            ui->label->setPixmap(newPixmap);

            QTimer::singleShot(50,this,SLOT(drawImage()));
            return;
        }
        else if (scrollY == 0) // show image only when not zoomed
        {
            ui->label->setPixmap(pixmap);

            if (pixmap.width() < rect.width())
            {
                rect.setLeft((rect.width()-pixmap.width())/2);
            }
            if (pixmap.height() < rect.height())
            {
                rect.setY((rect.height()-pixmap.height())/2);
            }

            ui->label->setGeometry(rect);
        }
    }

    if (slideShow)
    {
        QTimer::singleShot(DELAY_MILLI_SEC,this,SLOT(onNext()));
    }

    return;
}

void MainWindow::onZoomIn()
{
    slideShow = false;

    if (ui->label->pixmap() == NULL)
    {
        return;
    }

    if (nLoop < fileDialog.selectedFiles().count())
    {
        factor *= IMAGE_SCALE_RATIO;

        pixmap = QPixmap(fileDialog.selectedFiles()[nLoop]);

        int width = pixmap.width()*factor;
        int height = pixmap.height()*factor;
        pixmap = pixmap.scaled(
                    width,
                    height);

        scrollY = 0; // set the image at start position
        drawImage();
    }

    return;
}


void MainWindow::onZoomOut()
{
    slideShow = false;

    if (ui->label->pixmap() == NULL)
    {
        return;
    }

    if (nLoop < fileDialog.selectedFiles().count())
    {
        factor /= IMAGE_SCALE_RATIO;

        pixmap = QPixmap(fileDialog.selectedFiles()[nLoop]);
        pixmap = pixmap.scaled(
                    pixmap.width()*factor,
                    pixmap.height()*factor);

        scrollY = 0;
        drawImage();
    }

    return;
}

void MainWindow::onPlay()
{
    if (slideShow)
    {
        return;
    }

    slideShow = true;
    if (fileDialog.selectedFiles().count() > 0)
    {
        scrollY = 0;
        drawImage();
    }

    return;
}

void MainWindow::onPause()
{
    slideShow = false;

    return;
}

void MainWindow::onNext()
{
    nLoop++;
    factor = 1.0;
    scrollY = 0;

    if (nLoop >= fileDialog.selectedFiles().count())
    {
        nLoop = 0;
    }

    // show next only when there are images
    if (fileDialog.selectedFiles().count() > 0)
    {
        pixmap = QPixmap(fileDialog.selectedFiles()[nLoop]);
        drawImage();
    }

    return;
}

void MainWindow::onPrevious()
{
    nLoop--;
    factor = 1.0;
    scrollY = 0;

    if (nLoop < 0)
    {
        nLoop = fileDialog.selectedFiles().count() -1;
    }

    // show previous only when there are images
    if (nLoop >= 0 && fileDialog.selectedFiles().count() > 0)
    {
        pixmap = QPixmap(fileDialog.selectedFiles()[nLoop]);
        drawImage();
    }

    return;
}
