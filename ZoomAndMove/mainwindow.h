#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QFileDialog>
#include <QPixmap>

namespace Ui
{
    class MainWindowClass;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindowClass *ui;
    QFileDialog fileDialog;
    int nLoop;
    bool slideShow;
    QPixmap pixmap;
    double factor;
    int scrollY;

public slots:
    void onOpen();
    void onZoomIn();
    void onZoomOut();
    void onPlay();
    void onPause();
    void onNext();
    void onPrevious();
    void drawImage();
};

#endif // MAINWINDOW_H
